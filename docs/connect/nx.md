# NoMachine / NX

[NoMachine](https://www.nomachine.com/) (formerly NX) is a computer program that handles remote X Window
System connections and offers several performance improvements over traditional X11
forwarding. NoMachine can greatly improve the response time of X Windows and is 
the recommended method of interacting with GUIs and visualization tools running
on NERSC resources. NoMachine also allows a user to disconnect from a session 
and reconnect to it at a later time while keeping the state of all running
applications inside the session.

The current default environment on NERSC NoMachine nodes is GNOME desktop
running on CentOS linux. Most of the instructions on this page assume the 
use of the GNOME desktop environment. If you would prefer to use the 
lighter-weight Xfce desktop, please see [Use Xfce instead of GNOME
](#use-xfce-instead-of-gnome) further down this page.

## Getting Started with NoMachine

### Install the NoMachine Client

To use NoMachine at NERSC, you will first need to download and install the 
appropriate version of the [NoMachine Enterprise Client
](https://www.nomachine.com/download-enterprise#NoMachine-Enterprise-Client)
for the operating system on your laptop or workstation.

### Configure a NERSC connection

NoMachine at NERSC is hosted via the SSH protocol, so you must configure your 
connection to NERSC to authenticate via SSH.  This means either using your 
[NERSC password and one-time-code](../mfa/#using-mfa-with-ssh) every time you 
connect to NoMachine, or configuring [sshproxy](../mfa/#sshproxy) to generate 
a daily SSH key pair. If you don't want to reauthenticate with your password 
and a new one-time code every time the NoMachine Client is disconnected, 
using `sshproxy` is strongly recommended.  You will need to regenerate your
ssh key pair with sshproxy once a day, but once your key is generated the 
NoMachine Client will automatically connect.

To set up your first NoMachine connection, select your 
desired authentication method below and follow the instructions.

??? example "Authenticate via sshproxy (Mac or Linux)"

    Check out this video (note the Client version is old but the general settings
    remain the same) which shows you how to install NoMachine from scratch and
    configure using `sshproxy`.
    
    <iframe width="560" height="315"
    src="https://www.youtube.com/embed/sdlD6CUSE9o" frameborder="0"
    allow="autoplay; encrypted-media" allowfullscreen></iframe>
    
    1. Not shown in video but required: To enable `sshproxy` in NoMachine, you will need to edit one of the NoMachine
       config files on your local machine. **You must edit this file while NoMachine is
       closed/not running**. First exit the NoMachine
       program and then edit `$HOME/.nx/config/player.cfg` and
       change the following key from `library` to `native`: `<option key="SSH client mode" value="native" />`
    1. [Install sshproxy on your laptop and generate an SSH key](mfa.md#sshproxy).
       Note that you must do this once every day to generate a new key.
    1. Open the NoMachine client and click on the green Add icon in the upper left corner
    1. Under Address > Name the connection something like "Connection to NERSC"
    1. Under Address > Host must be `nxcloud01.nersc.gov`
    1. Under Address > Protocol set it to `ssh`. This should automatically change the Port to 22.
    1. Under Configuration > Authentication, select "use key-based authentication with a key you provide"
    1. Click Modify. Fill in the path to the ssh key you generated (usually
       `/Users/<yourusername>/.ssh/nersc`)
       and click the back error at the top left.
    1. Click the yellow Connect button at the top right. Enter your NERSC username.
    1. Verify the key. Create a new desktop. You should land at the NERSC homescreen.
    1. You can connect to Perlmutter via the green buttons on the left hand menu.
    1. To safely log out, click the `log out` button on the left hand menu of the
       GNOME desktop. If you click `x` without logging out, your session may hang
       which will prevent you from starting a new session. If you think this has
       happened to you, please open a ticket at the [NERSC Help Desk](https://help.nersc.gov)
       so we can help you reset your NoMachine access.
    1. To use this connection in the future, when you start the NoMachine client, just
       click in the icon you named "Connection to NERSC".

??? example "Authenticate via sshproxy (Windows)"

    1. To enable `sshproxy` in NoMachine, you will need to edit one of the NoMachine
       config files on your local machine. **You must edit this file while NoMachine is
       closed/not running**. Make sure you can view hidden files and
       navigate to `C:\Users\<yourusername>\.nx\config\player.cfg`. You can edit this file
       using Notepad++ or other programs that allow you to edit hidden files
       which cannot be edited by default in Windows.
       Change the following key from `library` to `native`: `<option key="SSH client mode" value="native" />`
    1. [Install sshproxy on your laptop and generate an SSH key](mfa.md#sshproxy).
       Note that you must do this once every day to generate a new key.
       Use this command to generate your key: `sshproxy.exe -u <yourusername> --format openssh -o nersc`
    1. Open the NoMachine client and click on the green Add icon in the upper left corner
    1. Under Address > Name the connection something like "Connection to NERSC"
    1. Under Address > Host must be `nxcloud01.nersc.gov`
    1. Under Address > Protocol set it to `ssh`. This should automatically change the Port to 22.
    1. Under Configuration > Authentication, select "use key-based authentication with a key you provide"
    1. Click Modify. Fill in the path to the ssh key you generated in step 1 (usually in the place you installed `sshproxy.exe`)
       Make sure you select `nersc` (rather than `nersc.pub`) as your private key. Click the back arrow at the top left.
    1. Click the yellow Connect button at the top right. Enter your NERSC username.
    1. Verify the key. Create a new desktop. You should land at the NERSC homescreen.
    1. You can connect to Perlmutter via the green buttons on the left hand menu.
    1. To safely log out, click the `log out` button on the left hand menu of the
       GNOME desktop. If you click `x` without logging out, your session may hang
       which will prevent you from starting a new session. If you think this has
       happened to you, please open a ticket at `help.nersc.gov` so we can help
       you reset your NoMachine access.
    1. To use this connection in the future, when you start the NoMachine client, just
       click in the icon you named "Connection to NERSC".

??? example "Authenticate with password and OTC"

    1. Open the NoMachine client and click on the green Add icon in the upper left corner
    1. Under Address > Name the connection something like "Connection to NERSC"
    1. Under Address > Host must be `nxcloud01.nersc.gov`
    1. Under Address > Protocol set it to `ssh`. This should automatically change the Port to 22.
    1. Under Configuration > Authentication, select "Use password authentication"
    1. Click the yellow Connect button in the top right corner
    1. Type your NERSC username and password+OTP. Don't save your password in the connection file; you will need to enter a new OTP every time you log on.
    1. Verify the key. Create a new desktop You should land at the NERSC homescreen.
    1. You can connect to Perlmutter via the green buttons on the left hand menu.
    1. To safely log out, click the `log out` button on the left hand menu of the
       GNOME desktop. If you click `x` without logging out, your session may hang
       which will prevent you from starting a new session. If you think this has
       happened to you, please open a ticket at `help.nersc.gov` so we can help
       you reset your NoMachine access.
    1. To use this connection in the future, when you start the NoMachine client, just
       click in the icon you named "Connection to NERSC".

### Verify host on first connection

Upon making your first connection to NERSC via the NoMachine client, you should 
see a message asking to verify the authenticity of the NERSC NoMachine portal:

![nomachine_fingerprint](images/nomachine_fingerprint.png)

The displayed key should match one of NERSC's 
[published key fingerprints for NX](../#nomachinenx).

!!! danger
    If you notice that the key that NoMachine displays does not match
    the [keys published on the NERSC
    website](index.md#key-fingerprints) , please stop and contact us
    immediately at the [NERSC Help Desk](https://help.nersc.gov).

## Advanced Configuration

Since the NoMachine portal provides a remote Linux desktop environment,
there are endless configuration customizations that can be made for
your preferences and productivity, but here are a few of the most
common configuration tweaks made by NERSC users:

??? tip "Add Custom SSH Options to NoMachine Connection"

    Since the NoMachine at NERSC is configured to use the SSH protocol to 
    authenticate, custom SSH connection options can be applied.  The
    easiest way to do this is to write a `ssh_config` file, commonly
    found at `~/.ssh/config` on Unix-like operating systems.

    For example, if your ssh-agent has too many keys, and you would
    like NoMachine to only use your `sshproxy` key located at `~/.ssh/nersc`, 
    your `ssh_config` might look like:

    ```
    host nxcloud01.nersc.gov
      identityfile ~/.ssh/nersc
      identitiesonly yes
    ```

    This has the convenient side-effect of letting you easily test a
    direct SSH connection to the NoMachine server (see 
    [Troubleshooting NX Connection Failures](#troubleshooting-nx-connection-failures)
    below).  In this case, you may want to add additional settings so you can 
    easily `ssh` to `nxcloud01.nersc.gov` via command line:

    ```
    host nxcloud
      hostname nxcloud01.nersc.gov
      user <your-nersc-username>
      addkeystoagent yes
      forwardagent yes
      identityfile ~/.ssh/nersc
      identitiesonly yes
    ```

    !!! caution "Be careful with SSH aliases"
        If you assign an ssh host alias to `nxcloud01.nersc.gov` make sure that
        you use the same alias in the connection settings in the NoMachine client!

??? tip "Use Xfce instead of GNOME"

    If you find the [GNOME desktop environment](https://www.gnome.org) to be laggy 
    or display graphics with poor resolution, or you would simply prefer an alternative
    lighter-weight environment, we recommend trying [Xfce](https://www.xfce.org):
    
    1. Make sure you have completely logged out of any open NoMachine
       sessions. To do so click `log out` in the bottom left corner of the
       GNOME desktop menu.
    1. Return to the NoMachine connection page. Create a new connection file following
       either the with/without sshproxy directions above. To help your future self,
       consider calling it `Xfce connection` or something similar.
    1. Click connect.
    1. Once you've connected, choose `Create a new custom session`
    1. On the next screen, click `Run the following command` and fill in
       `/bin/startxfce4` and click `Run the command in a virtual desktop`
    1. This should give you a session with the Xfce desktop
    1. Then you can connect to Perlmutter by clicking on the small terminal icon on
       the bottom of the screen and typing `ssh -X perlmutter-p1.nersc.gov`.
    1. To log out of Xfce, click on the box with your name at the top right corner.
       Select `log out`. Then click `log out` again. It may take several seconds
       to fully close your session. There is also a `log out` option
       under the upper left menu `Applications/Others` but this log out button
       does not currently work.

??? tip "Resize the NoMachine screen"

    With the latest NoMachine Player (5.0.63 or later), the most efficient way is
    to enable "Remote Resize" in the NoMachine menu:
    
    1. Connect to NoMachine
    1. From the desktop, bring up the NoMachine player menu with a hotkey: Mac:
       Ctrl+Option+0, Windows: Ctrl+Alt+0, Linux: Ctrl+Alt+0
    1. Choose the "Display" submenu, then toggle the "Remote Resize"
       button. You can also choose "Change Settings" to manually change
       the resolution.

??? tip "Change terminal font size"

    To change the font size inside your terminal: In the menu of Konsole
    Application, choose "Settings"->"Manage Profiles", then click "Edit
    Profile...", now you can change the font size in the "Appearance" tab,
    after changing, click "OK" until you are back to the terminal. Now
    every new terminal window you open will have the new font size.
    
    To change the font size of your menu bars/window titles: Right click
    on an empty desktop then choose "Konsole", inside the Konsole, type
    "kcmshell4 fonts". Then you have a dialog box to change your font
    size.
    
??? tip "Customize your NoMachine terminal"

    If you dislike the default NoMachine terminal color scheme (white background, dark
    text), you can open a terminal from the `Show Applications` menu at the bottom
    of the menu bar on the left hand side of the screen (it looks like 9 dots).
    Once you have opened your terminal, click `Edit` --> `Preferences` in the
    terminal menu. If you click the `Colors` menu at the top of the window, you can
    uncheck the `Use colors from system theme` box. You can then choose the color
    of the background and text you prefer.
    
    If you like to make adjustments from the command line, open a
    terminal/konsole from the `Show Applications` menu and enter:
    
    ```shell
    dconf write /org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/use-theme-colors false
    ```
    
    This should permanently adjust your terminal color settings to use a
    dark background with light text.
    
??? tip "Adjust keyboard language"

    To change your keyboard language from the default (US English), click the `Show
    Applications` menu at the bottom of the menu bar on the left hand side of the
    screen (it looks like 9 dots). Then click the `Settings` icon --> `Region and
    Language`. Under `Input Sources`, click the plus arrow on the bottom left hand
    side. This should open a list of additional options (for example: `Spanish
    (Spain)`). Under this menu will be another, more detailed submenu with options
    such as `Spanish (Macintosh)`. Choose your option and click the `Add` button on
    the upper right hand side. Then under input sources, make sure your new option
    is highlighted. You can click the keyboard icon on the bottom right to see how
    the keys in your new configuration are arranged.

## Troubleshooting NX Connection Failures

If you are having trouble connecting to NoMachine, please try these steps first:

1. Log into [Iris](https://iris.nersc.gov) to clear any login
   failures. Access to NoMachine uses your NERSC user name and password. If
   your password is mistyped five times, NERSC will lock you out of
   Our systems. Logging into Iris will automatically clear these
   failures. This will also let you know if your password is expired
   (which would prevent you from accessing NoMachine, among many other
   things).

1. Create a new connection file following [the instructions
   above](#configure-a-nersc-connection). NX will often
   "update" the configuration file to try to save your settings and
   occasionally incorrect settings can be saved. You must have the
   new NoMachine player AND an updated configuration file to connect
   to the NoMachine service.

1. Try to ssh directly to the NoMachine server. This will help to 
   distinguish if you are having a connection issue that is specific to
   NoMachine or a general SSH connection issue. You can do this with the
   command <br />
   `ssh <nersc_username>@nxcloud01.nersc.gov` <br />
   and your NERSC user name and password+one-time MFA password 
   (with no spaces in between). If your access to the NoMachine server 
   is blocked by a local firewall or something else and you can't 
   connect via ssh, you will also not be able to connect with the 
   NoMachine client. If this is the case, please contact your 
   local IT department.

1. If you're using sshproxy, test the sshproxy key by using it to `ssh`
   to the NoMachine server. You can do this with the command <br />
   `ssh -i ~/.ssh/nersc <nersc_username>@nxcloud01.nersc.gov` <br />
   If this fails and the previous step works, there may be a problem with your
   sshproxy key, please open a ticket with NERSC to diagnose the issue.

If you've tried these steps and still cannot connect, please open a
help ticket. In this ticket, please include the following information:

* Your operating system, NoMachine client version, and whether you are connecting with
   or without sshproxy.
* Screen capture/text of any error messages received.
* Copy of your local NoMachine client connection file, likely
   located at `Documents/NoMachine/<name of your connection>.nxs`.
* If NERSC staff request them, a tarball of your NoMachine logs.
   You can find instructions for how to
   bundle your NoMachine logs on
   the [NoMachine website](https://www.nomachine.com/DT07M00098).
