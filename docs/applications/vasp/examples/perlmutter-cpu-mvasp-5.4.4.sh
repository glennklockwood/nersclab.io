#!/bin/bash
#SBATCH -C cpu
#SBATCH --qos=debug
#SBATCH --time=0:30:00
#SBATCH --nodes=2
#SBATCH --error=mvasp-%j.err
#SBATCH --output=mvasp-%j.out

module load mvasp/5.4.4-cpu

sbcast --compress=lz4 `which mvasp_std` mvasp_std
srun -n128 -c4 --cpu-bind=cores mvasp_std

