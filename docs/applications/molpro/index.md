# MOLPRO 

[MOLPRO](https://www.molpro.net/) is a complete system of ab initio
programs for molecular electronic structure calculations, written and
maintained by H.-J. Werner and P. J. Knowles, with contributions from
several other authors. As distinct from other commonly used quantum
chemistry packages, the emphasis is on highly accurate computations,
with extensive treatment of the electron correlation problem through
the multiconfiguration-reference CI, coupled cluster, and associated
methods. Using recently developed integral-direct local electron
correlation methods, which significantly reduce the increase of the
computational cost with molecular size, accurate ab initio
calculations can be performed for much larger molecules than with most
other programs.

The heart of the program consists of the multiconfiguration SCF,
multireference CI, and coupled-cluster routines, and these are
accompanied by a full set of supporting features.

## Availability and Supported Architectures at NERSC

As of 06/01/2023 MOLPRO is not supported at NERSC. Licensed users are welcome to make their own builds.

## Application Information, Documentation, and Support

[MOLPRO User's manual](https://www.molpro.net/manual/doku.php)

## Related Applications

* [GAMESS](../gamess/index.md)
* [Q-chem](../qchem/index.md)
* [NWChem](../nwchem/index.md)

## User Contributed Information

!!! info "Please help us improve this page"
	Users are invited to contribute helpful information and corrections
	through our [GitLab repository](https://gitlab.com/NERSC/nersc.gitlab.io/blob/main/CONTRIBUTING.md).
