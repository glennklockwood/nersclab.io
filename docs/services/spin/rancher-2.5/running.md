# Running Your App in Spin

Log into https://rancher2.spin.nersc.gov/, select the environment (development or production) and navigate to your NERSC
project. In Spin, a project is the highest level of encapsulation, matching the projects you have access to in Iris. If
you don't see your project in Spin, please contact NERSC staff.

A project contains namespaces, which is the next level of encapsulation for your Spin applications. Typically, a Spin
application will run in a single namespace. 

Find an existing namespace or create a new one inside your project. This is where your application will live in Spin.

## Deploy a workload in Spin

Assuming you have created the docker images and pushed them to the registry, you can now deploy your app using the
Rancher UI. In your namespace, click "Deploy". What you're doing here is telling Spin to create "Deployments" (also
called "workloads"). A Deployment is a collection of Pods.

The image name should be the url of your docker image in the registry, for example:

```
registry.nersc.gov/<myproject>/<myimage>:<mytag>
```

Fill in any other settings your application needs, for example the entrypoint, env vars, etc.

## Linux Capability Requirements

Spin has special rules about running apps in order to allow multiple users to use shared resources. Click `Show advanced
options` to see all the settings.

Under `Security and Host Config`, **drop "all" capabilities** and allow only the ones you actually need. Spin will only
allow you to specify these capabilities: `CHOWN`, `KILL`, `SETGID`, `SETUID`, `NET_BIND_SERVICE`, `DAC_OVERRIDE`,
`FOWNER`.

**Important:** if you mount an external volume (more on this [later](../storage/)) you can only select the
`NET_BIND_SERVICE` capability.

## Verify the deployment

If everything went well, Rancher will deploy your workflow which will create a pod. A pod is set of containers, where a
container is an instance of a Docker image. For a typical microservices application a pod could for example be an
`nginx` web server running in a container.

Click the pod name to view the running containers inside the pod. Next to the container name on the right hand side is a
drop-down menu (click the three dots). Select `Execute shell` which will open a shell inside the container. For example,
if you deployed a database image, you can run the database client from this shell to make sure everything worked.

If something failed, you can see the error in red above the pod name, or in the namespace/pod logs.
