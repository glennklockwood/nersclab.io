# Versioning

The development cycle for the Superfacility API is designed to
maintain maximum stability for research workflows while at the same
time allowing for new features to be added quickly and transparently.

To meet these requirements, the API has both a _protocol version_ and
a _software version_.

## Protocol version

The protocol version of the API is embedded in the URL syntax; for
example, `https://api.nersc.gov/api/v1.2/`.

Changes to the protocol version are rare; they would only increment if
non-backwards-compatible features are added. (An example of this
would be wholesale changes to the request or response payload of an
endpoint.) API changes that are simply _additive_ -- in other words,
new features and bug fixes -- the protocol version (and the URL) will
remain the same.

If the API protocol version is upgraded, the previous version will
remain operational for 12 months.

## Software version

The software version of the API is denoted by a code in the changelog
based on the release date of the change. You can view the software version
changes via the `/meta/changelog` API.

Each released software version will generate a changelog
entry. Software versions may cycle rapidly during periods of active
development; nominally, new versions are released on an approximately
weekly interval, with the exception of emergency bug fixes.
