import json
import sys

infile_name  = sys.argv[1]
outfile_name = sys.argv[2]

with open(infile_name, "r") as f:
    index = json.load(f)
    # keys are config and docs
    new_docs = [ page for page in index['docs']
                 if 'translations/zh' not in page['location'] ]
    index['docs'] = new_docs
    with open(outfile_name, "w") as outfile:
        json.dump(index, outfile)
